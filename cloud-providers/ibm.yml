.ibm:
  tags: ['osdu-medium']
  image: openshift/origin-cli
  environment:
    name: IBM

  variables:
    IBM_DEPLOY_DIR: ibm_deploy_dir
    IBM_OPENSHIFT_PROJECT: og-cicd

  before_script:
    - mkdir -p "$IBM_DEPLOY_DIR"
    - oc login $IBM_OPENSHIFT_URL -u apikey -p $IBM_OPENSHIFT_TOKEN

ibm-deploy:
  extends: .ibm
  stage: deploy
  needs: ['compile-and-unit-test']
  script:
    - cp -v ${IBM_BUILD_SUBDIR}/target/*-spring-boot.jar "$IBM_DEPLOY_DIR"
    - cd "$IBM_DEPLOY_DIR"
    - oc project $IBM_OPENSHIFT_PROJECT
    - oc get bc/$CI_PROJECT_NAME 2> /dev/null || oc new-build --name $CI_PROJECT_NAME --binary --strategy source --image-stream openshift/java:8
    - oc start-build $CI_PROJECT_NAME --from-dir=. --follow
    - oc get service $CI_PROJECT_NAME 2> /dev/null || oc new-app $CI_PROJECT_NAME
  rules:
    - if: $IBM_SKIP_DEPLOY == 'true'
      when: never
    - if: $IBM_BUILD_SUBDIR && $IBM == 'true' && $IBM_OPENSHIFT_URL && $IBM_OPENSHIFT_TOKEN

.ibm_variables:

  variables:
    # COMMON TEST
    DOMAIN: $IBM_DOMAIN
    ENTITLEMENTS_DOMAIN: $IBM_DOMAIN
    DEFAULT_DATA_PARTITION_ID_TENANT1: opendes # must be opendes to work
    DEFAULT_DATA_PARTITION_ID_TENANT2: tenant2
    LEGAL_TAG: opendes-public-usa-dataset-1 # must exist in target instance
    OTHER_RELEVANT_DATA_COUNTRIES: US

    # Services
    SEARCH_HOST: $IBM_SEARCH_HOST
    STORAGE_HOST: $IBM_STORAGE_HOST
    DELIVERY_HOST: $IBM_DELIVERY_HOST
    SCHEMA_HOST: $IBM_SCHEMA_HOST

    # common security
    KEYCLOAK_URL: $IBM_KEYCLOAK_URL
    KEYCLOAK_REALM: OSDU
    KEYCLOAK_CLIENT_ID: $IBM_KEYCLOAK_CLIENT_ID
    KEYCLOAK_CLIENT_SECRET: $IBM_KEYCLOAK_CLIENT_SECRET

    AUTH_USER_ACCESS: $IBM_AUTH_USER_ACCESS
    AUTH_USER_ACCESS_PASSWORD: $IBM_AUTH_USER_ACCESS_PASSWORD
    AUTH_USER_NO_ACCESS: $IBM_AUTH_USER_NO_ACCESS
    AUTH_USER_NO_ACCESS_PASSWORD: $IBM_AUTH_USER_NO_ACCESS_PASSWORD

    # SEARCH TEST
    USER_EMAIL: $IBM_KEYCLOAK_USER_EMAIL
    ELASTIC_HOST: $IBM_ELASTIC_HOST
    ELASTIC_USER_NAME: $IBM_ELASTIC_USER_NAME
    ELASTIC_PASSWORD: $IBM_ELASTIC_PASSWORD
    ELASTIC_PORT: $IBM_ELASTIC_PORT

    # Storage
    PROJECT_ID: 1
    DEPLOY_ENV: empty
    TENANT_NAME: opendes
    PUBSUB_TOKEN: token_not_implemented
    STORAGE_URL: $IBM_STORAGE_HOST
    LEGAL_URL: $IBM_LEGAL_HOST
    ENTITLEMENT_URL: $IBM_ENTITLEMENT_URL

    # Delivery
    IBM_COS_ENDPOINT: $IBM_COS_ENDPOINT
    IBM_COS_REGION: $IBM_COS_REGION
    IBM_COS_ACCESS_KEY: $IBM_COS_ACCESS_KEY
    IBM_COS_SECRET_KEY: $IBM_COS_SECRET_KEY

    # Legal
    HOST_URL: $IBM_LEGAL_HOST
    MY_TENANT: $TENANT_NAME
    MY_TENANT_PROJECT: OpenDES_Project
    IBM_ENV_PREFIX: $IBM_ENV_PREFIX
    IBM_LEGAL_MQ_CONNECTION: $IBM_LEGAL_MQ_CONNECTION
    
    # Schema
    VENDOR: ibm
    HOST: $IBM_SCHEMA_HOST
    PRIVATE_TENANT1: $DEFAULT_DATA_PARTITION_ID_TENANT1
    PRIVATE_TENANT2: $DEFAULT_DATA_PARTITION_ID_TENANT2
    SHARED_TENANT: $IBM_SHARED_TENANT

    # Workflow
    FINISHED_WORKFLOW_ID: $IBM_FINISHED_WORKFLOW_ID
    WORKFLOW_HOST: $IBM_WORKFLOW_HOST
    
    # File
    FILE_SERVICE_HOST: $IBM_FILE_SERVICE_HOST
    DATA_PARTITION_ID: $IBM_DATA_PARTITION_ID

    # Unit
    VIRTUAL_SERVICE_HOST_NAME: $IBM_UNIT_SERVICE_HOST

    # Ingest
    INGEST_HOST: $IBM_INGEST_HOST
    TEST_FILE_ID: $IBM_TEST_FILE_ID
    TEST_OSDU_FILE_PATH: $IBM_TEST_OSDU_FILE_PATH
    
    #Notification
    ENVIRONMENT: $IBM_ENVIRONMENT
    HMAC_SECRET: $IBM_HMAC_SECRET
    REGISTER_BASE_URL: $IBM_REGISTER_BASE_URL
    NOTIFICATION_BASE_URL: $IBM_NOTIFICATION_BASE_URL
    AUTH_USER_ADMINS_ACCESS: $IBM_AUTH_USER_ADMINS_ACCESS
    AUTH_USER_ADMINS_ACCESS_PASSWORD: $IBM_AUTH_USER_ADMINS_ACCESS_PASSWORD
    AUTH_USER_EDITORS_ACCESS: $IBM_AUTH_USER_EDITORS_ACCESS
    AUTH_USER_EDITORS_ACCESS_PASSWORD: $IBM_AUTH_USER_EDITORS_ACCESS_PASSWORD
    REGISTER_CUSTOM_PUSH_URL_HMAC: $IBM_REGISTER_CUSTOM_PUSH_URL_HMAC
    NOTIFICATION_REGISTER_BASE_URL: $IBM_NOTIFICATION_REGISTER_BASE_URL
    #Registration
    SUBSCRIPTION_ID: $IBM_SUBSCRIPTION_ID
    SUBSCRIBER_SECRET: $IBM_HMAC_SECRET
    

    #Indexer
    INDEXER_API_KEY: $IBM_INDEXER_API_KEY
    INDEXER_HOST_URL: $IBM_INDEXER_HOST_URL

    #Catalog
    IBM_VIRTUAL_HOST_CRS_CATALOG: $IBM_CATALOG_SERVICE_HOST

    #Conversion
    IBM_VIRTUAL_HOST_CRS_CONVERSION: $IBM_CONVERSION_SERVICE_HOST

ibm-test:
  stage: integration
  needs: ["ibm-deploy"]
  # allow_failure: false
  extends:
    - .maven
    - .ibm_variables
  script:
    - if [[ $IBM_INT_TEST_SUBDIR == *"ibm"* ]]; then $MAVEN install -f ${IBM_INT_TEST_SUBDIR/ibm/core}/pom.xml -DskipTests=true; fi
    - export TEST_CMD=${IBM_TEST_CMD:-test}
    - $MAVEN $TEST_CMD -q -f $IBM_INT_TEST_SUBDIR/pom.xml > test-results.log
  artifacts:
    when: always
    paths:
      - test-results.log
      - $IBM_INT_TEST_SUBDIR/target/*/TEST-*.xml
    reports:
      junit:
        - $IBM_INT_TEST_SUBDIR/target/*/TEST-*.xml
  only:
    variables:
      - $IBM_INT_TEST_SUBDIR && $IBM == 'true'
  except:
    variables:
      - $IBM_SKIP_TEST == 'true'

ibm-test-py:
  stage: integration
  image: python:3.6
  needs: ["ibm-deploy"]
  # allow_failure: true
  extends:
    - .ibm_variables
  script:
    - pip install --upgrade pip
    - cd $IBM_INT_TEST_PY_SUBDIR
    - pip install -r requirements.txt
    - python ${IBM_INT_TEST_PY_FILE:-test.py}
  # artifacts:
  #   when: always
  #   paths:
  #     - $IBM_INT_TEST_SUBDIR/target/*/TEST-*.xml
  #   reports:
  #     junit:
  #       - $IBM_INT_TEST_SUBDIR/target/*/TEST-*.xml
  only:
    variables:
      - $IBM_INT_TEST_PY_SUBDIR && $IBM == 'true'
  except:
    variables:
      - $IBM_SKIP_TEST == 'true'