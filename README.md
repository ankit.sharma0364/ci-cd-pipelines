# OSDU CI/CD Pipeline Overview

Find the most recent documentation build [here](http://osdu.pages.community.opengroup.org/platform/ci-cd-pipelines/)

A common place for shared CI/CD Pipeline files, for use will all OSDU projects.


[The OSDU Platform project](https://community.opengroup.org/osdu/platform) (including sub-projects) makes use of [GitLab's built-in CI/CD capabilities](https://docs.gitlab.com/ee/ci/introduction/) to provide a CI/CD pipeline that runs automatically on commits to the project. The stages in the pipeline include:

- Security scanning for known vulnerabilities (CVEs)
- Scanning for use of approved Licenses in submitted code and dependencies
- Correct attribution of the submitted material
- Unit tests
- Integration tests

## Common Practices / Definitions

**Continuous Integration**
* Developers merge/integrate the code to Master as soon as developer made a progress on the feature. Usually everyone commits to the Master every day.
* Continuous Integration assumes a high degree of tests which are automated into the software. we have good automated testing coverage as we progress we need to add more test cases.
* Continuous Integration refers to Integrating code, building and testing code (Unit and Integration tests) within the development environment.

**Continuous Delivery**

* Continuous Delivery means that you are able to do frequent deployments to a testing environment and/or production environment but may choose not to do it, due to various reasons (Ex: businesses not ready or OSDU applicatiions not ready)

**Continuous Deployment**

* Continuous Deployment means that every change goes through the pipeline and automatically (No manual approval) gets put into production, resulting in many production deployments every day.


## A graphical overview of the CI/CD Pipeline:

**Continuous Delivery is not ready and its just an idea for now but we will have to figure out how & where to run UAT and Performance tests.**

**Continuous Deployment might not be applicable for OSDU.**

![alt text](CD1.jpg "CI/CD Pipeline")
